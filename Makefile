NAME=$(shell basename `git rev-parse --show-toplevel`)
DIFF=$(shell git diff-index --name-only HEAD)

all: get add commit pull push

get:
ifeq ($(DIFF), )
	git pull
endif

add:
	git add .

commit:
	git commit -m "Updated $(NAME) on `sh getname.sh`"

pull:
	git pull --rebase

push:
	git push
