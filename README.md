# simple-git-sync

This project was originally born out of a simple desire to synchronize my [Joplin notes](https://joplin.cozic.net/) through `git`, rather than using a proprietary service.

The concept is similar to [SparkleShare](https://www.sparkleshare.org/), but much, much simpler.

## Usage

For this to work, you'll need `git` (obviously), GNU Make (other makes may work, but I haven't tried), and cron. If you're using this, you probably already have these on your computer.

You must have a git repo set up locally, which can push and pull to its remote without any interaction. This might be done with SSH keys, or, if using HTTPS, a `.netrc` file. You'll need to figure this part out yourself.

Next, copy `Makefile` and `getname.sh` to the root of your git repo.

Now, set up a job to run make regularly. I do every five minutes, by adding the following to my cronjob.

    */5 * * * * make -C /path/to/repo

### With Joplin

I set Joplin to sync with my filesystem, and pointed it to a git repo. Now Joplin syncs to that folder, and then `git` pushes all the changes to my remote. On other machines, `git` automatically pulls those changes and Joplin syncs with them.

### Setup on Android

You might notice that `getname.sh` checks if the current device is Anddroid, and if so, prints the device name, rather than the hostname.

Using this on Android requires some additional tools to set it up.

You'll need the following tools:

- [Tasker](https://tasker.joaoapps.com/)
- [Termux](https://termux.com/)
- [Termux:Task plugin](https://wiki.termux.com/wiki/Termux:Task)

If you're going to be using this to use repos with Android apps, such as Joplin or [Orgzly](http://www.orgzly.com/), you'll need these repos stored somewhere these apps can read.

So, after starting Termux the first time, run `termux-setup-storage` to setup symlinks to your main storage.

You'll probably then have to install `git` and `make`, by running `pkg install git make`

Next, clone your repos somewhere under ~/storage/shared or ~/storage/downloads. Personally, I have ~/storage/shared/repos, with the individual repos under that.

I did the following:

```shell
mkdir storage/shared/repos
cd storage/shared/repos
git clone git@somegit.com:username/repo.git
```

Since you've already setup the necessary files within your git repo, you won't need to add annything now.

Next, you'll need a script for Tasker to run. Ther Termux:Task plugin expects these scripts to be under ~/.termux/tasker. At this point, you'll need to install some text editor, or create this file on your computer and copy it over.

The script should have something like this:

```shell
#!/bin/bash

make -C ~/storage/shared/repos/mydir
```

Obviously, you'll adjust the path to whatever you chose.

Termux doesn't install stuff in standard locations, so that shebang won't actually work. Termux provides a utility to fix shebangs to work on your phone. If you've put this script under ~/.termux/tasker/sync.sh, just run `termux-fix-shebang ~/.termux/tasker/sync.sh`, and then make sure to make it executable with `chmod +x ~/.termux/tasker/sync.sh`.

You're almost there. The only thing left is to setup Tasker.

You can just import the provided Profile. Copy Sync.prof.xml to your phone, then on the "Profiles" tab, long press on the tab itself until a dropdown appears and tap "Import". Navigate to the file, select it, and it should appear in the list. Now, your sync script will run ever five minutes.

If that doesn't work for you, you can do the following to manually add the task:

- From Profiles, click "+"
- Select "Time"
    + For "From", choose 12:00am (midnight)
	+ For "To", choose 11:59pm (1 minute before midnight)
	+ For "Every", choose 5 minutes
- Click "New Task", and if you'd like, give it a name
- Click "+", and select "Plugin" and the "Termux:Task"
    + Tap on the pencil next to "Configuration"
	+ Under "Executable", type "sync.sh" (or whatever you named the script)
	+ Click the save icon
- Click the back icon twice

Now you're done!

## Warning

I make no claim as to the safety of this. It could completely destroy your files, cheat on your spouse, and ruin your life. You assume all liability by using this software.

I've been using it for a while, and it's never caused me a problem. But I don't claim it'll be fine for you.

Be aware that this is very naive. It assumes everything is hunky-dokey. It doesn't resolve merge conflicts for you. If there are merge conflicts, you'll have to fix them yourself the old fashioned way. But, as long as you're not using the repos on multiple computers at the exact same time, you'll probably be ok, but again, I claim no responsibility if it's not.
