#!/bin/sh

if [ "$(uname -o)" = "Android" ]; then
    echo `getprop ro.product.manufacturer; getprop ro.product.model`
else
    uname -n
fi
